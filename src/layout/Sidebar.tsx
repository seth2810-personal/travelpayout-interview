import React from 'react';

import { ReactComponent as LogoIcon } from '../shared/icons/logo.svg';
import { ReactComponent as CircleIcon } from '../shared/icons/circle.svg';

import styles from './Sidebar.module.css';

const MenuItem = ({ active = false }) => (
    <div className={`${styles.menuItem} ${active && styles.menuItemActive}`}>
        <CircleIcon/>
    </div>
);

const Sidebar = () => (
    <aside className={styles.sidebar}>
        <div className={styles.logo}>
            <LogoIcon/>
        </div>
        <nav className={styles.menu}>
            <MenuItem/>
            <MenuItem/>
            <MenuItem/>
            <MenuItem/>
            <MenuItem/>
            <MenuItem/>
            <MenuItem active={true}/>
            <MenuItem/>
        </nav>
    </aside>
);

export default Sidebar;