import React from 'react';

import Statistics from '../shared/Statistics';

import styles from './Header.module.css';

interface IHeaderProps {
    balance: number;
    currency: string;
    next_payout: number;
}

export default class Header extends React.PureComponent<IHeaderProps> {
    render() {
        return (
            <header className={styles.header}>
                <Statistics title="Баланс" text={`${this.props.balance.toLocaleString('ru-RU')} ₽`}/>
                <Statistics title="К выплате" text={`${this.props.next_payout.toLocaleString('ru-RU')} ₽`}/>
            </header>
        )
    }
}
