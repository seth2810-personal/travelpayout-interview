import React from 'react';

import Header from './layout/Header';
import Footer from './layout/Footer';
import Sidebar from './layout/Sidebar';
import Bonuses from './bonuses/Bonuses';

import data from './shared/data.json';
import styles from './App.module.css';

const App = () => (
    <section className={styles.app}>
        <div className={styles.sidebar}>
            <Sidebar/>
        </div>
        <div className={styles.header}>
            <Header {...data.header}/>
        </div>
        <div className={styles.content}>
            <Bonuses bonuses={data.bonuses}/>
        </div>
        <div className={styles.footer}>
            <Footer/>
        </div>
    </section>
);

export default App;