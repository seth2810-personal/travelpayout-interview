import React, { SyntheticEvent, ElementType } from 'react';

import { IBonus } from './Bonus.interface';

import Input from '../shared/Input';

import Bonus from './Bonus';

import styles from './Bonuses.module.css';

interface IBonusesProps {
    bonuses: IBonus[];
}

interface IBonusesState {
    filterValue: string | null;
}

export default class Bonuses extends React.PureComponent<IBonusesProps, IBonusesState> {
    constructor(props: IBonusesProps) {
        super(props);

        this.state = {
            filterValue: null
        };
    }
    
    onFilterChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ filterValue: event.target.value.toLowerCase() });
    }

    onFilterClear = () => {
        this.setState({ filterValue: null });
    }

    get bonuses() {
        const { filterValue } = this.state;
        const { bonuses } = this.props;

        if (!filterValue) {
            return bonuses;
        }

        return bonuses.filter(({ title }) => title.toLowerCase().includes(filterValue))
    }

    render() {
        return (
            <React.Fragment>
                <h1 className={styles.header}>Сервисы</h1>
                <div className={styles.filter}>
                    <div className={styles.input}>
                        <Input
                            label="Фильтр"
                            onChange={this.onFilterChange}
                            type="text" value={this.state.filterValue || ''}
                        />
                    </div>
                    <button className={styles.clearFiler} onClick={this.onFilterClear}>Сбросить</button>
                </div>
                {this.bonuses.map((bonus, index) => <Bonus key={index} bonus={bonus}/>)}
            </React.Fragment>
        );
    }
}