import React from 'react';

import Input from '../shared/Input';
import { ReactComponent as CopyIcon } from '../shared/icons/copy.svg';

import { IBonus } from './Bonus.interface';

import styles from './Bonus.module.css';

interface IBonusProps {
    bonus: IBonus;
}

export default class Bonus extends React.PureComponent<IBonusProps> {
    inputRef: React.RefObject<HTMLInputElement>;
    
    constructor(props: IBonusProps) {
        super(props);

        this.inputRef = React.createRef();
    }

    onCopy = () => {
        const { current: input } = this.inputRef; 

        if (input) {
            input.select();
            document.execCommand('copy');
            input.selectionEnd = input.selectionStart;
            input.blur();
        }
    }
    
    render() {
        const { bonus } = this.props;

        return (
            <div className={styles.container}>
                <div className={styles.title}>{bonus.title}</div>
                <div className={styles.description}>{bonus.description}</div>
                <div className={styles.promocode}>
                    <Input className={styles.promocodeInput} type="text" value={bonus.promocode} label="Промокод"
                        inputRef={this.inputRef} onClick={this.onCopy} readOnly/>
                    <CopyIcon className={styles.promocodeCopy}/>
                </div>
                <a className={styles.promocodeGet} href={bonus.link} target="_blank">Получить бонус</a>
            </div>
        );
    }
}