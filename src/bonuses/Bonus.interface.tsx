export interface IBonus {
    link: string;
    title: string;
    promocode?: string;
    description?: string;
}