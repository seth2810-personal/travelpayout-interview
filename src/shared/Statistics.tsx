import React from 'react';

import styles from './Statistics.module.css';

interface IStatisticsProps {
    title: string;
    text: string;
}

export default class Statistics extends React.PureComponent<IStatisticsProps> {
    render() {
        return (
            <div className={styles.container}>
                <div className={styles.title}>{this.props.title}</div>
                <div className={styles.text}>{this.props.text}</div>
            </div>
        );
    }
}