import React from 'react';

import styles from './Input.module.css';

interface IInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
    label?: string;
    action?: React.ComponentType;
    inputRef?: React.RefObject<HTMLInputElement>;
}

export default class Input extends React.PureComponent<IInputProps> {
    render() {
        const { label, inputRef, ...props } = this.props;

        return (
            <div className={styles.container}>
                {label && <label className={styles.label}>{label}</label>}
                <input {...props} ref={inputRef}/>
            </div>
        );
    }
}